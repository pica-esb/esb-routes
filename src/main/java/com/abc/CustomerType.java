package com.abc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerType {
	private Integer Id;
	
	private String Name;

	@JsonProperty("Id")
	public Integer getId() {
		return Id;
	}

	@JsonProperty("Id")
	public void setId(Integer id) {
		Id = id;
	}

	@JsonProperty("Name")
	public String getName() {
		return Name;
	}

	@JsonProperty("Name")
	public void setName(String name) {
		Name = name;
	}	
}
