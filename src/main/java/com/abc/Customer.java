package com.abc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Customer {
	
	private Integer Id;
	
	private String FirstName;
	
	private String LastName;
	
	private String PhoneNumber;
	
	private String Email;
	
	private String IdentificationCardType;
	
	private String IdentificationCard;
	
	private CustomerType Type;
	
	@JsonProperty("Id")	
	public Integer getId() {
		return Id;
	}

	@JsonProperty("Id")
	public void setId(Integer id) {
		Id = id;
	}

	@JsonProperty("FirstName")
	public String getFirstName() {
		return FirstName;
	}
	
	@JsonProperty("FirstName")
	public void setFirstName(String firstName) {
		this.FirstName = firstName;
	}
	
	@JsonProperty("LastName")
	public String getLastName() {
		return LastName;
	}
	
	@JsonProperty("LastName")
	public void setLastName(String lastName) {
		this.LastName = lastName;
	}
	
	@JsonProperty("PhoneNumber")
	public String getPhoneNumber() {
		return PhoneNumber;
	}
	
	@JsonProperty("PhoneNumber")
	public void setPhoneNumber(String phoneNumber) {
		this.PhoneNumber = phoneNumber;
	}
	
	@JsonProperty("Email")
	public String getEmail() {
		return Email;
	}
	
	@JsonProperty("Email")
	public void setEmail(String email) {
		this.Email = email;
	}
	
	@JsonProperty("IdentificationCardType")
	public String getIdentificationCardType() {
		return IdentificationCardType;
	}
	
	@JsonProperty("IdentificationCardType")
	public void setIdentificationCardType(String identificationCardType) {
		this.IdentificationCardType = identificationCardType;
	}
	
	@JsonProperty("IdentificationCard")
	public String getIdentificationCard() {
		return IdentificationCard;
	}
	
	@JsonProperty("IdentificationCard")
	public void setIdentificationCard(String identificationCard) {
		this.IdentificationCard = identificationCard;
	}

	@JsonProperty("Type")
	public CustomerType getType() {
		return Type;
	}

	@JsonProperty("Type")
	public void setType(CustomerType type) {
		Type = type;
	}
	
	
}
