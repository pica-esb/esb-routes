package com.abc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerErrorResponse {
	
	private Integer Code;
	
	private String Description;

	@JsonProperty("Code")
	public Integer getCode() {
		return Code;
	}

	@JsonProperty("Code")
	public void setCode(Integer code) {
		Code = code;
	}

	@JsonProperty("Description")
	public String getDescription() {
		return Description;
	}

	@JsonProperty("Description")
	public void setDescription(String description) {
		Description = description;
	}
}
